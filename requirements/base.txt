django>=1.9

django-extensions>=1.5.2
django-appconf>=1.0
django-storages==1.4.1
django-import-export>=0.3.1
django-compressor>=1.5

# rest framework
djangorestframework>=3.1.1
drf-extensions>=0.2.7
djangorestframework-jwt==1.4.2

# requests
requests==2.9.1

# uwsgi
uWSGI>=2.0.10

# database
psycopg2>=2.6

# image processing
Pillow==3.4.2

# auth
django-allauth==0.29.0

