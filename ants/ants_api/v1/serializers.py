# -*- coding: utf-8 -*-

import logging
import datetime
import traceback

from django.contrib.auth import get_user_model
import rest_framework.serializers as serializers

from ants_core.models import UserRecord, Service
__author__ = 'keshang'


class URServiceSerializer(serializers.ModelSerializer):
    logo = serializers.URLField(read_only=True)

    class Meta:
        model = Service
        fields = (
            'name',
            'slug',
            'logo',
        )


class URUserSerializer(serializers.ModelSerializer):
    class Meta:
        model = get_user_model()
        fields = (
            'username',
            'email',
        )


class UserRecordListSerializer(serializers.ModelSerializer):
    user = URUserSerializer()
    service = URServiceSerializer()

    class Meta:
        model = UserRecord
        fields = (
            'id',
            'user',
            'service',
            'created_at',
            'fee_amount',
            'fee_frequency',
        )


class UserRecordCreateSerializer(serializers.ModelSerializer):

    class Meta:
        model = UserRecord
        fields = (
            'id',
            'user',
            'service',
            'created_at',
            'fee_amount',
            'fee_frequency',
        )


class ServiceListSerializer(serializers.ModelSerializer):
    class Meta:
        model = Service
        fields = (
            'pk',
            'name',
            'slug',
            'logo',
        )


class AppUserSignupFromFacebookSerializer(serializers.Serializer):
    # the short-lived access token
    access_token = serializers.CharField(required=True)

    # the second that the access token is going to expire
    expires_in = serializers.IntegerField(required=True)

    # if by any reason we cannot get user's email from the access token,
    # we would expect ``email`` field to be filled so that we can continue the registration
    email = serializers.EmailField(required=False)

    # ``timestamp``, ``appId`` and ``hash`` are used to make sure the request comes from the authorized app
    timestamp = serializers.IntegerField()
    app_id = serializers.CharField()
    hash = serializers.CharField()

    # ``os`` and ``deviceId`` help to track the user's device
    os = serializers.CharField()
    device_id = serializers.CharField()

    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop('request', None)
        super(AppUserSignupFromFacebookSerializer, self).__init__(*args, **kwargs)

    def validate(self, attrs):

        if not settings.DEBUG:
            app_setting = settings.API_APP_SETTINGS.get(attrs['app_id'])
            secret_key = app_setting['secret']

            hash_str = hashlib.sha1(
                str(attrs['access_token']) + str(attrs['timestamp']) + str(attrs['app_id']) + str(secret_key)
            ).hexdigest()

            if hash_str != attrs['hash']:
                raise api_exceptions.InvalidApp

        # if client passed the manual email to server, check the uniqueness, raise
        if 'email' in attrs:
            if core_system.models.User.objects.filter(email=attrs['email']).exists():
                raise api_exceptions.FacebookLoginEmailTaken

        return attrs

