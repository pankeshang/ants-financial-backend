# -*- coding: utf-8 -*-

import logging
import datetime
import traceback

from django.conf.urls import include, url
from django.conf import settings

import ants_api.v1.views as api_v1


__author__ = 'keshang'

urlpatterns = [
    url(
        '^sign-in/oauth/$',
        api_v1.OAuthSignInSignUpView.as_view(),
        name='sign-in-oauth/'
    ),
    url('^sign-out/$',
        api_v1.SignOutView.as_view(),
        name='sign-out'),

    url('^services/$',
        api_v1.ServiceListView.as_view(),
        name='list-services'),

    url('^me/records/$',
        api_v1.MyRecordsView.as_view(),
        name='list-user-records'),

    url('^me/records/(?P<rid>\d+)/$',
        api_v1.MyRecordDetailView.as_view(),
        name='crud-user-record'),
]
