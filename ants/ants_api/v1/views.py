# -*- coding: utf-8 -*-
import json
import logging
import datetime
import traceback
import requests

import django.http as http
from django.core.urlresolvers import reverse
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.utils import timezone
from django.core.exceptions import ObjectDoesNotExist
from django.contrib.auth import get_user_model, logout

from rest_framework.views import View, APIView
from rest_framework.generics import ListCreateAPIView, RetrieveUpdateDestroyAPIView, ListAPIView
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework.response import Response
from rest_framework.exceptions import APIException

from allauth.socialaccount.models import (SocialLogin, SocialToken)
from allauth.socialaccount.helpers import complete_social_login
from allauth.socialaccount import providers
from allauth.socialaccount.providers.facebook.provider import FacebookProvider, GRAPH_API_URL
from allauth.socialaccount.providers.facebook.views import fb_complete_login

from rest_framework_jwt.settings import api_settings
jwt_payload_handler = api_settings.JWT_PAYLOAD_HANDLER
jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER

from .serializers import (
    UserRecordListSerializer,
    UserRecordCreateSerializer,
    ServiceListSerializer,
    AppUserSignupFromFacebookSerializer
)

from ants_core.models import UserRecord, Service
__author__ = 'keshang'

logger = logging.getLogger(__name__)


# ------------------------
#        Records
# ------------------------
class MyRecordsView(ListCreateAPIView):
    permission_classes = (IsAuthenticated, )
    queryset = UserRecord.objects.all()

    list_serializer_class = UserRecordListSerializer
    create_serializer_class = UserRecordCreateSerializer

    def get_serializer_class(self):
        """
        for GET, return self.list_serializer_class
        for POST, return self.create_serializer_class
        """
        return self.list_serializer_class if self.request.method == 'GET' else self.create_serializer_class


class MyRecordDetailView(RetrieveUpdateDestroyAPIView):
    permission_classes = (IsAuthenticated, )
    queryset = UserRecord.objects.all()
    lookup_url_kwarg = 'rid'
    lookup_field = 'pk'
    serializer_class = UserRecordCreateSerializer


# ------------------------
#        Services
# ------------------------
class ServiceListView(ListAPIView):
    permission_classes = (IsAuthenticated,)

    queryset = Service.objects.all()
    serializer_class = ServiceListSerializer


# ---------------------------------
#        Signup/Login/Logout
# ---------------------------------
class OAuthSignInSignUpView(View):
    pass


class SignOutView(View):
    def post(self, request):
        if not request.user.is_authenticated():
            return http.HttpResponseBadRequest('You are not logged in')

        logout(request)

        if request.is_ajax():
            return http.HttpResponse(json.dumps([]), headers={'context-type': 'application/json'})
        else:
            return http.HttpResponseRedirect(reverse('index'))


class AppUserLoginByFacebookView(APIView):
    permission_classes = [AllowAny, ]

    @method_decorator(csrf_exempt)
    def post(self, request):
        """
        Same as normal app user login, but instead of username, email and password, client
        pass the access_token, expires_in and email(optional) to the server.

        We inject an app-flag into request so that the server knows there this user is from

        - User sign up using facebook
            - granted email permission ==> create new user
            - denied email ==> pop out the message box, ask email again. If user still denies, cancel the process

        - User signed up from using the same email as his facebook account before,
            and later decide to login from facebook"
            - granted email permission ==> find the existing user based on his email address and then link it up
                                            with the facebook record at the backend
            - denied email ==> pop out the message box, ask email again. If user still denies, cancle the process

        :param request:
        :return:
        """

        # serialize request data
        srz = AppUserSignupFromFacebookSerializer(data=request.data)
        srz.is_valid(raise_exception=True)
        data = srz.validated_data
        # get the short-lived access_token from request
        access_token = data['access_token']

        ret = None
        auth_exception = None

        # ---------------------------
        #    start hacking allauth
        # ---------------------------

        # get the facebook provider
        provider = providers.registry.by_id(FacebookProvider.id)

        # get the facebook app
        app = provider.get_app(request)

        # exchange for a long-lived token
        try:
            resp = requests.get(
                GRAPH_API_URL + '/oauth/access_token',
                params={'grant_type': 'fb_exchange_token',
                        'client_id': app.client_id,
                        'client_secret': app.secret,
                        'fb_exchange_token': access_token}
            ).json()
        except requests.RequestException as e:
            raise APIException('cannot exchange the long term token: {}'.format(e))

        # create social token object
        token = SocialToken({
            'app': app,
            'token': resp['access_token'],
            'expires_at': timezone.now() + datetime.timedelta(seconds=resp['expires_in'])
        })

        # calls GRAPH API /me to get the user data and do ``sociallogin`` using the response
        social_login = fb_complete_login(request, app, token)
        social_login.token = token
        social_login.state = SocialLogin.state_from_request(request)

        # hack the request._request, put the ``from_mobile_app`` flag in for ``SocialAccountAdapter`` to use
        request._request.from_mobile_app = True

        # django-allauth module called the django message framework inside
        # complete_social_login, which only take django.http.HttpRequest object but not the DRF's
        # request object, hence we pass the ``request._request``, which is the original request
        # object
        ret = complete_social_login(request._request, social_login)

        # make sure user is saved here before we link it up with device.
        try:
            existing_user = get_user_model().objects.get(email=social_login.user.email)

            if not social_login.user.pk:
                # There is already an ants.financial user with the same email as the facebook user, link them up
                social_login.user = existing_user
                social_login.save(request)
            else:
                social_login.user.save()
        except ObjectDoesNotExist:
            social_login.user.save()

        # create a JWT token
        payload = jwt_payload_handler(social_login.user)
        return {
            'token': jwt_encode_handler(payload),
            'user': social_login.user
        }

        return Response({'token': token.key})

