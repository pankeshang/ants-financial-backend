# -*- coding: utf-8 -*-
import logging
import datetime
import traceback

__author__ = 'keshang'


from django.conf.urls import include, url
from django.contrib import admin
from django.conf import settings

urlpatterns = [
    url(r'^v1/', include('ants_api.v1.urls', namespace='v1')),
]
