# -*- coding: utf-8 -*-
from django.contrib import admin
from django.conf import settings
from django.conf.urls import include, url
from django.conf.urls.static import static
import django.views.static

from rest_framework_jwt.views import obtain_jwt_token

import ants_core.views

urlpatterns = [

    url(r'^$', ants_core.views.index, name='index'),

    url(r'^admin/', include(admin.site.urls)),

    url(r'^core/', include('ants_core.urls', namespace='ants-core')),

    url(r'^api/', include('ants_api.urls', namespace='ants-api')),

    # http://django-allauth.readthedocs.org/en/latest/installation.html#django
    # url('^accounts/facebook/login/token/$', user.UserFacebookLoginByTokenView.as_view(), name='allauth-accounts-facebook-login-by-token'),
    url(r'^accounts/', include('allauth.urls'), ),

    # django rest framework jtw username/password auth (for dev purpose)
    url(r'^api-token-auth/', obtain_jwt_token),

]

# static files
urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)

# media files
urlpatterns += [
    url(r'^media/(?P<path>.*)$', django.views.static.serve, {'document_root': settings.MEDIA_ROOT})
]


