# -*- coding: utf-8 -*-

import logging
import datetime
import traceback
from .base import *

__author__ = 'keshang'

DEBUG = False

TESTING = False

ALLOWED_HOSTS = [
    'trilinq.truuue.com',
]

DOMAIN = 'trilinq.truuue.com'

env.read_env('.env')



# ---------------------------------------
#                Database
# ---------------------------------------
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'ants_financial',
        'USER': 'halfamonk',
        'PASSWORD': 'halfamonk',
        'HOST': 'localhost',
        'PORT': '5432',
    },

}


# ---------------------------------------
#                 Logging
# ---------------------------------------
LOGS_ROOT = '/tmp/'

LOGGING_FORMATTERS = {
    'verbose': {
        'format': ('%(levelname)s [%(asctime)s] %(process)d '
                   '[%(name)s.%(module)s.%(funcName)s] (%(lineno)d): %(message)s'
                   '\n\n\n'),
        'datefmt': '%Y-%m-%d %H:%M:%S'
    },
    'standard': {
        'format': ('%(levelname)s [%(asctime)s] %(process)d '
                   '[%(name)s.%(module)s.%(funcName)s] (%(lineno)d): %(message)s'),
        'datefmt': '%Y-%m-%d %H:%M:%S'
    },
    'simple': {
        'format': '%(levelname)s [%(name)s.%(module)s.%(funcName)s] %(message)s'
    },
}

LOGGING_FILTERS = {
    'require_debug_false': {
        '()': 'django.utils.log.RequireDebugFalse'
    }
}

LOGGING_HANDLERS = {
    'mail_admins': {
        'level': 'ERROR',
        'filters': ['require_debug_false'],
        'class': 'django.utils.log.AdminEmailHandler'
    },
    'console': {
        'level': 'DEBUG',
        'class': 'logging.StreamHandler',
        'formatter': 'simple'
    },
    'db_logging': {
        'level': 'DEBUG',
        'class': 'logging.handlers.RotatingFileHandler',
        'backupCount': 5,
        'maxBytes': 5000000,
        'filename': os.path.join(LOGS_ROOT, '%s.django-db.log' % DOMAIN)
    },
    'request_logging': {
        'level': 'DEBUG',
        'class': 'logging.handlers.RotatingFileHandler',
        'backupCount': 5,
        'filename': os.path.join(LOGS_ROOT, '%s.request.log' % DOMAIN),
        'formatter': 'standard',
    },
}

LOGGING_LOGGERS = {
    'django': {
        'handlers': ['console'], 'level': 'INFO', 'propagate': True,
    },
    'django.request': {
        'handlers': ['mail_admins', 'console', ], 'level': 'INFO', 'propagate': True,
    },
    'django.db': {
        'handlers': ['db_logging'], 'level': 'DEBUG', 'propagate': False,
    },
    'default': {
        'handlers': ['request_logging', 'console'], 'level': 'DEBUG', 'propagate': False,
    },
    'test': {
        'handlers': ['console'], 'level': 'DEBUG', 'propagate': False,
    },
}

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': LOGGING_FORMATTERS,
    'filters': LOGGING_FILTERS,
    'handlers': LOGGING_HANDLERS,
    'loggers': LOGGING_LOGGERS,
}


# ---------------------------------------
#              Compressor
# ---------------------------------------
COMPRESS_ENABLED = True
COMPRESS_DEBUG_TOGGLE = 'debug'
COMPRESS_OFFLINE = False
STATICFILES_FINDERS = (
    "django.contrib.staticfiles.finders.FileSystemFinder",
    "django.contrib.staticfiles.finders.AppDirectoriesFinder",
    "compressor.finders.CompressorFinder",
)
COMPRESS_PRECOMPILERS = (
    ('text/coffeescript', 'coffee --compile --stdio'),
    ('text/less', 'lessc {infile} {outfile}'),
    ('text/x-sass', 'sass {infile} {outfile}'),
    ('text/x-scss', 'sass --scss {infile} {outfile}'),
    ('text/stylus', 'stylus < {infile} > {outfile}'),
    ('text/foobar', 'path.to.MyPrecompilerFilter'),
)

COMPRESS_OUTPUT_DIR = ''

