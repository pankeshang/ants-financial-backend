# -*- coding: utf-8 -*-
from .base import *

import logging
import datetime
import traceback

__author__ = 'keshang'

DEBUG = False

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': '/Users/admin/bidlah/data/thetrilinq.sqlite.test.db',
    },
}


# rewrite ``MIGRATION_MODULES`` to a un-existed directory to fool the system
# and hence bypass the migration process, which can speed up the test db creation by... a lot
MIGRATION_MODULES = {}
for i in INSTALLED_APPS:
    MIGRATION_MODULES[i] = '%s.migrations_not_used_in_tests' % i

TEST_RUNNER = 'django_nose.NoseTestSuiteRunner'


INSTALLED_APPS = tuple(list(INSTALLED_APPS) + [
    'django_nose',
])

NOSE_ARGS = [
    '--nocapture',
    '--nologcapture',
    # '--with-coverage',
    # '--cover-package=browse,truuuemagic,reports,api,core_system,push_notifications',
    # '--cover-html',
    # '--verbosity=2'
]


# --------- Compressor -----------
COMPRESS_ENABLED = False
