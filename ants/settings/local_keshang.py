# -*- coding: utf-8 -*-

import logging
import datetime
import traceback
from .base import *

__author__ = 'keshang'


DEBUG = True


# ---------------------------------------
#                Database
# ---------------------------------------
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'ants',
        'USER': 'halfamonk',
        'PASSWORD': 'halfamonk',
        'HOST': 'localhost',
        'PORT': '5432',
    },
}

# ---------------------------------------
#              Compressor
# ---------------------------------------
COMPRESS_ENABLED = False


# ---------------------------------------
#              Hoiio
# ---------------------------------------
HOIIO_APPID = ''
HOIIO_TOKEN = ''


# ---------------------------------------
#              Mandrill
# ---------------------------------------
MANDRILL_API_KEY = ''
