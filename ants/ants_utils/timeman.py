# -*- coding: utf-8 -*-

import logging
import datetime
import traceback
import re
import time
import datetime
import calendar
import pytz

__author__ = 'keshang'


class TimeMan():
    TIME_PATTERN = re.compile('\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2}')
    DATE_PATTERN = re.compile('\d{4}-\d{2}-\d{2}')
    @staticmethod
    def UTC_time_str_to_UTC_obj(in_str):
        result = TimeMan.TIME_PATTERN.match(in_str)
        if result:
            return datetime.datetime.strptime(in_str, '%Y-%m-%d %H:%M:%S')

    @staticmethod
    def UTC_date_str_to_UTC_obj(in_str):
        result = TimeMan.DATE_PATTERN.match(in_str)
        if result:
            return datetime.datetime.strptime(in_str, '%Y-%m-%d')

    @staticmethod
    def SG_time_str_to_SG_obj(in_str):
        return TimeMan.UTC_time_str_to_UTC_obj(in_str)

    @staticmethod
    def SG_date_str_to_SG_obj(in_str):
        return TimeMan.UTC_date_str_to_UTC_obj(in_str)

    @staticmethod
    def SG_time_str_to_UTC_obj(in_str):
        return TimeMan.UTC_time_str_to_UTC_obj(in_str) - datetime.timedelta(hours=8)

    @staticmethod
    def SG_date_str_to_UTC_obj(in_str):
        return TimeMan.UTC_date_str_to_UTC_obj(in_str) - datetime.timedelta(hours=8)

    @staticmethod
    def SG_obj_to_int(in_obj):
        return TimeMan.UTC_obj_to_int(in_obj - datetime.timedelta(hours=8))

    @staticmethod
    def UTC_obj_to_int(in_dateObjUTC):
        return int(calendar.timegm(in_dateObjUTC.timetuple()))

    @staticmethod
    def int_to_UTC_obj(in_int):
        a = time.gmtime(in_int)
        return datetime.datetime.fromtimestamp(time.mktime(a))

    @staticmethod
    def int_to_SG_obj(in_int):
        return TimeMan.int_to_UTC_obj(in_int) + datetime.timedelta(hours=8)

    @staticmethod
    def SG_time_str_to_int(in_str):
        return TimeMan.SG_obj_to_int(TimeMan.SG_time_str_to_SG_obj(in_str))

    @staticmethod
    def SG_date_str_to_int(in_str):
        return TimeMan.SG_obj_to_int(TimeMan.SG_date_str_to_SG_obj(in_str))

    @staticmethod
    def get_o_today():
        return datetime.datetime.combine(datetime.datetime.now(), datetime.time())

    @staticmethod
    def month_to_quarter(month):
        return (month - 1) // 3 + 1

    # ------- 2014-01 Timezone related function -----------
    # *not timezone-awared datetime objects*
    @staticmethod
    def dt_to_int(dt_obj):
        """ with tzinfo
            >>> import pytz
            >>> from TimeMan import TimeMan
            >>> import datetime

            >>> sg = pytz.timezone('Asia/Singapore')
            >>> tr = pytz.timezone('Europe/Istanbul')

            >>> o_today = datetime.datetime(2014,3,30)
            >>> tr_today = tr.localize(o_today)
            >>> tr_today
            datetime.datetime(2014, 3, 30, 0, 0, tzinfo=<DstTzInfo 'Europe/Istanbul' EET+2:00:00 STD>)
            >>> print tr_today
            2014-03-30 00:00:00+02:00

            >>> tr_tmr = tr.localize(o_today + datetime.timedelta(days=1))
            >>> print tr_tmr
            2014-03-31 00:00:00+03:00

            >>> start_int = TimeMan.dt_to_int(tr_today)
            >>> print 'start_int:', start_int
            start_int: 1396130400

            >>> end_int = TimeMan.dt_to_int(tr_tmr)
            >>> print 'end_int:', end_int
            end_int: 1396213200

        """
        return int(calendar.timegm(dt_obj.utctimetuple()))

    @staticmethod
    def dt_sg_to_int(dt_obj):
        # add the timezone info to the ``dt_obj`` if the tzinfo is not set
        if not dt_obj.tzinfo:
            sg = pytz.timezone('Asia/Singapore')
            dt = sg.localize(dt_obj)
        else:
            dt = dt_obj
        return TimeMan.dt_to_int(dt)

    @staticmethod
    def dt_tr_to_int(dt_obj):
        sg = pytz.timezone('Europe/Istanbul')
        dt = sg.localize(dt_obj)
        return TimeMan.dt_to_int(dt)

    @staticmethod
    def dt_nz_to_int(dt_obj):
        sg = pytz.timezone('Pacific/Auckland')
        dt = sg.localize(dt_obj)
        return TimeMan.dt_to_int(dt)

    @staticmethod
    def dt_utc_to_int(dt_obj):
        sg = pytz.timezone('Europe/Istanbul')
        dt = sg.localize(dt_obj)
        return TimeMan.dt_to_int(dt)

    @staticmethod
    def int_to_dt(in_int, tz_str='UTC'):
        """ support tzinfo
            >>> int_a = 13962841600
            >>> TimeMan.int_to_dt_utc(int_a)
            >>> print TimeMan.int_to_dt_utc(int_a)
            2014-03-31 16:00:00
            >>> print TimeMan.int_to_dt_sg(int_a)
            2014-04-01 00:00:00
            >>> print TimeMan.int_to_dt_tr(int_a)
            2014-03-31 19:00:00
            >>> print TimeMan.int_to_dt(int_a,tz_str="Asia/Bangkok")
            2014-03-31 23:00:00
        """
        # Get UTC dt
        dt = datetime.datetime.fromtimestamp(time.mktime(time.gmtime(in_int)))
        # pytz.timezone.utcoffset(dt) returns a timedelta object
        dt += pytz.timezone(tz_str).utcoffset(dt)
        return dt

    @staticmethod
    def int_to_dt_utc(in_int):
        return TimeMan.int_to_dt(in_int, tz_str="UTC")

    @staticmethod
    def int_to_dt_sg(in_int):
        return TimeMan.int_to_dt(in_int, tz_str="Asia/Singapore")

    @staticmethod
    def int_to_dt_tr(in_int):
        return TimeMan.int_to_dt(in_int, tz_str="Europe/Istanbul")

    @staticmethod
    def add_tz_to_dt(dt_obj, tz_str='Asia/Singapore'):
        '''
        apply timezone data to a timezone-inawared datetime object
        '''
        if not dt_obj.tzinfo:
            tz = pytz.timezone(tz_str)
            return tz.localize(dt_obj)

    #------- 2014-02 Timezone related function -----------
    # change from one timezone to another by timezone names or country code
    # *not timezone-awared datetime objects*
    @staticmethod
    def give_me_dt(in_dt, from_tz=None, to_tz=None):
        if not from_tz or not to_tz:
            raise ValueError("Please specify from_tz and to_tz")
        from_tz_obj = pytz.timezone(from_tz)
        to_tz_obj = pytz.timezone(to_tz)

        utc_offset_from_tz = from_tz_obj.utcoffset(in_dt)
        utc_offset_to_tz = to_tz_obj.utcoffset(in_dt)
        return in_dt - utc_offset_from_tz + utc_offset_to_tz

    @staticmethod
    def give_me_dt_country_code(in_dt, from_country=None, to_country=None):
        """
            # Singapore datetime ( no tzinfo )
            ... dt = datetime.datetime(2014,2,26,10,0,0)
            >>> dt
            datetime.datetime(2014, 2, 26, 10, 0)

            >>> foo = TimeMan.give_me_dt(dt, from_tz='Asia/Singapore', to_tz='Pacific/Auckland')
            >>> foo
            datetime.datetime(2014, 2, 26, 15, 0)

            >>> bar =  TimeMan.give_me_dt_country_code(dt, from_country='SG', to_country='NZ')
            >>> bar
            datetime.datetime(2014, 2, 26, 15, 0)

            # Singapore datetime ( no tzinfo ), after DST
            >>> dt = datetime.datetime(2014,6,1,0,0)
            >>> dt
            datetime.datetime(2014, 6, 1, 0, 0)

            >>> foo = TimeMan.give_me_dt(dt, from_tz='Asia/Singapore', to_tz='Pacific/Auckland')
            >>> foo
            datetime.datetime(2014, 6, 1, 4, 0)

            >>> bar = TimeMan.give_me_dt_country_code(dt, from_country='SG', to_country='NZ')
            >>> bar
            datetime.datetime(2014, 6, 1, 4, 0)
        """
        if not from_country or not to_country:
            raise ValueError("Please specify from_country and to_country")
        from_tz = pytz.country_timezones[from_country][0]
        to_tz = pytz.country_timezones[to_country][0]
        return TimeMan.give_me_dt(in_dt, from_tz=from_tz, to_tz=to_tz)
