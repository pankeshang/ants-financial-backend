# -*- coding: utf-8 -*-

import logging
import urllib
import datetime
import traceback
import requests

from django.conf import settings

__author__ = 'keshang'

logger = logging.getLogger(__name__)


class HoiioService(object):
    API_SEND_SMS = 'https://secure.hoiio.com/open/sms/send'
    API_QUERY_SMS_STATUS = 'https://secure.hoiio.com/open/sms/query_status'

    def __init__(self, app_id, access_token, sender_name=None):
        self.app_id = app_id
        self.access_token = access_token
        if sender_name:
            self.sender_name = sender_name
        else:
            self.sender_name = 'Truuue'

    def send_sms(self, dest, msg, sender_name=None):
        query_dict = {
            'app_id': self.app_id,
            'access_token': self.access_token,
            'sender_name': sender_name if sender_name else self.sender_name,
            'dest': dest,
            'msg': msg,
        }
        resp = requests.post(self.API_SEND_SMS, query_dict)

        logger.info('[SMS sent][From:{sender_name}][To:{dest}][Msg:{msg}][Resp:{resp.status_code}][Resp content:{resp.content}]'.format(
            sender_name=sender_name,
            dest=dest,
            msg=msg,
            resp=resp
        ))

        return resp

    def query_sms_status(self, tex_ref):
        query_dict = {
            'app_id': self.app_id,
            'access_token': self.access_token,
            'txn_ref': tex_ref,
        }
        url = ''
        resp = requests.get(self.API_QUERY_SMS_STATUS + '?{}'.format(urllib.urlencode(query_dict)))
        return resp


hoiio = HoiioService(settings.HOIIO_APPID, settings.HOIIO_TOKEN)

