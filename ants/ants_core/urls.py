# -*- coding: utf-8 -*-

import logging
import datetime
import traceback

__author__ = 'keshang'


from django.conf.urls import include, url
import ants_core.views


urlpatterns = [

    # create record
    url('^user/records/$',
        ants_core.views.UserRecordsView.as_view(),
        name='user-records'),

]
