# -*- coding: utf-8 -*-

import logging
import datetime
import traceback

from django.contrib import admin
from ants_core.models import Service, UserRecord

__author__ = 'keshang'


class ServiceAdmin(admin.ModelAdmin):
    pass


class UserRecordAdmin(admin.ModelAdmin):
    pass


admin.site.register(Service, ServiceAdmin)
admin.site.register(UserRecord, UserRecordAdmin)
