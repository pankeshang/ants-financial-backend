# -*- coding: utf-8 -*-

import logging
import datetime
import traceback
from django.apps import AppConfig

__author__ = 'keshang'


class AntsCoreConfig(AppConfig):
    name = 'ants_core'
    verbose_name = 'Ants Core'

