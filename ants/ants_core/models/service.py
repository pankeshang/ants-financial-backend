# -*- coding: utf-8 -*-

import logging
import datetime
import traceback

from django.core.exceptions import ObjectDoesNotExist
from django.db import models
from django.conf import settings
from django.utils import timezone
from django.utils.text import slugify
from django.utils.translation import ugettext as _


__author__ = 'keshang'


class Service(models.Model):

    created_at = models.DateTimeField(auto_now_add=True)

    name = models.CharField(max_length=50)

    slug = models.SlugField(max_length=50, allow_unicode=True, editable=False)

    logo = models.ImageField(
        upload_to='uploads/logos/',
        blank=True,
        null=True)

    background_color = models.CharField(
        max_length=6,
        blank=True,
        null=True,
        help_text='the 6 digit color code'
    )

    class Meta:
        verbose_name = _('Service')
        verbose_name_plural = _('Services')
        ordering = ('-pk',)

    def __str__(self):
        return self.slug

    def save(self, *args, **kwargs):

        if not self.pk:
            self.slug = slugify(self.name)

        super(Service, self).save(*args, **kwargs)

