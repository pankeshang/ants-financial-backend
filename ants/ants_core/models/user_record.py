# -*- coding: utf-8 -*-

import logging
import datetime
import traceback

from django.core.exceptions import ObjectDoesNotExist
from django.db import models
from django.conf import settings
from django.utils import timezone
from django.utils.text import slugify
from django.utils.translation import ugettext as _


__author__ = 'keshang'


FREQUENCY_CHOICES = (
    ('D', _('Daily')),
    ('W', _('Weekly')),
    ('M', _('Monthly')),
    ('Q', _('Quarterly')),
    ('Y', _('Annually')),
)


class UserRecord(models.Model):

    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name='tags',
        related_query_name='tag'
    )

    service = models.ForeignKey('ants_core.Service')

    created_at = models.DateTimeField(auto_now_add=True)

    fee_amount = models.IntegerField(verbose_name=_('Fee Amount'))

    fee_frequency = models.CharField(max_length=1, choices=FREQUENCY_CHOICES, db_index=True, verbose_name=_('Frequency'))

    class Meta:
        verbose_name = _('User Record')
        verbose_name_plural = _('User Records')
        ordering = ('-pk',)

    def __str__ (self):
        return u'{} - {}'.format(self.user.username, self.service.slug)



