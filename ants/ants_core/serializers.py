# -*- coding: utf-8 -*-

import logging
import datetime
import traceback

import rest_framework.serializers as serializers
import ants_core.models as models

__author__ = 'keshang'


class ServiceSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Service
        fields = (
            'name',
            'slug',
            'logo',
        )


class UserRecordSerializer(serializers.ModelSerializer):
    service = ServiceSerializer()

    class Meta:
        model = models.UserRecord
        fields = (
            'service',
            'created_at',
            'fee_amount',
            'fee_frequency',
        )