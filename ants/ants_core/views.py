# -*- coding: utf-8 -*-

import logging
import datetime
import traceback

from django.conf import settings
from django.core.exceptions import ValidationError, ObjectDoesNotExist
import django.http as http
from django.shortcuts import render
from django.views.generic import View
from django.views.generic import View

from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework.response import Response
from rest_framework.views import APIView
import rest_framework.status as status

from ants_core.serializers import ServiceSerializer, UserRecordSerializer
import ants_core.models as models


__author__ = 'keshang'


def index(request):
    return render(request, 'index.html', {})


class UserRecordsView(APIView):
    """
    Used to show user's record in browser
    """
    serializer_class = UserRecordSerializer

    def get(self, request):
        return Response(UserRecordSerializer(request.user.tags.all(), many=True).data)

    def post(self, request):
        slug = request.data['slug']
        try:
            service = models.Service.objects.get(slug=slug)
        except ObjectDoesNotExist:
            raise http.Http404

        user_record, created = models.UserRecord.objects.get_or_created(
            user=request.user,
            service=service
        )

        if not created:
            status = 200
        else:
            status = 201
        return Response(UserRecordSerializer(user_record).data, status=status)

    def delete(self, request):
        try:
            user_record = models.UserRecord.objects.get(pk=request.data['id'])
        except ObjectDoesNotExist:
            return Response({}, status=status.HTTP_404_NOT_FOUND)

        if not user_record.user == request.user:
            return Response([], status=status.HTTP_403_FORBIDDEN)

        models.UserRecord.objects.filter(pk=request.data['id']).delete()
        return Response([], status=status.HTTP_204_NO_CONTENT)


