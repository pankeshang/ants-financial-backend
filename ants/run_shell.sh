#!/bin/bash

# python manage.py test --_settings=thetrilinq.settings_keshang browse
if [[ $# -eq 0 ]]
then
  python manage.py shell --settings=settings.local_keshang
elif [ $1='show_urls' ]
then
  python manage.py show_urls --format=table --settings=settings.local_keshang | grep -v '/admin'
else
  python manage.py "$@" --settings=settings.local_keshang
fi

