# -*- coding: utf-8 -*-
import sys
import logging
import datetime
import traceback


import fabric.api
from fabric.contrib.files import append, exists, comment, contains
from fabric.contrib.files import upload_template as orig_upload_template


__author__ = 'keshang'

if len(sys.argv) < 2:
    print 'use fab --list to see the stage and tasks available'
    sys.exit(127)


logger = logging.getLogger('fabric')
logger.setLevel(logging.DEBUG)
settings_local_handler = logging.StreamHandler()
settings_local_handler.setFormatter(logging.Formatter(fmt='[FABRIC] %(message)s'))
logger.addHandler(settings_local_handler)

# Hack from http://stackoverflow.com/questions/2276200/changing-default-encoding-of-python
reload(sys)  # Reload does the trick!
sys.setdefaultencoding('UTF8')
